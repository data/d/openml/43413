# OpenML dataset: Store-20-Retail-Data-Analytics

https://www.openml.org/d/43413

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

About
This dataset is based on https://www.kaggle.com/manjeetsingh/retaildataset 
Historical sales data covers the period from 2010-02-05 to 2012-11-01. Sales and Features data was merged, Weekly sales were estimated at total store level, and only data for Store = 20 was selected.
Columns

Date - the week
Temperature - average temperature in the region
Fuel_Price - cost of fuel in the region
- MarkDown1-5 - anonymized data related to promotional markdowns. MarkDown data is only available after Nov 2011, and is not available for all stores all the time. Any missing value is marked with an NA
CPI - the consumer price index
Unemployment - the unemployment rate
IsHoliday - whether the week is a special holiday week
Weekly_Sales -  sales in the given date (week)

The original dataset was obtained here: 
https://www.kaggle.com/manjeetsingh/retaildataset

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43413) of an [OpenML dataset](https://www.openml.org/d/43413). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43413/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43413/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43413/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

